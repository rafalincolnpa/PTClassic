-- 메시 하나 출력
Begin("Parent");
InitPos(0, 20, 0);
End();

Begin("Mesh");
InitMeshName("Res\\Object\\MG_4-42-20.ASE");
InitMaxFrame(20);
InitLoop(1);
InitColor(255,255,255,255);
EventFadeColor(0,0,0,0,0);
EventFadeColor(0.2,255,255,255,200);
EventFadeColor(0.4,0,0,0,0);
End();

Begin("Mesh");
InitMeshName("Res\\Object\\MG_4-42-20.ASE");
InitMaxFrame(20);
InitLoop(1);
InitColor(255,255,255,255);
InitStartDelayTime(0.05);
EventFadeColor(0,0,0,0,0);
EventFadeColor(0.2,255,255,255,200);
EventFadeColor(0.4,0,0,0,0);
End();

Begin("Mesh");
InitMeshName("Res\\Object\\MG_4-42-20.ASE");
InitMaxFrame(20);
InitLoop(1);
InitColor(255,255,255,255);
InitStartDelayTime(0.1);
EventFadeColor(0,0,0,0,0);
EventFadeColor(0.2,255,255,255,200);
EventFadeColor(0.4,0,0,0,0);
End();




