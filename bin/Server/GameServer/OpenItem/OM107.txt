//		아이템 정보		//


*이름		"홀리 오브"
*Name		"Holy Orb"
*코드		"OM107"

///////////////	공통사항		 ////////////

*내구력		46 60
*무게		5
*가격		16000

///////////////	원소		/////////////

*생체		2 5
*불		2 5
*냉기		2 5
*번개		2 5
*독		2 5

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		0.7 1.099
*방어력		18 30
*블럭율		

//////////////	신발성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생
*근력재생
*생명력추가
*기력추가	12	18
*근력추가
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		30
*힘		
*정신력		60
*재능		
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화랜덤 Priestess Magician

// Prayer Shaman//

**흡수력	0.5 0.799
**기력재생	0.5 0.799	


*연결파일	"name\OM107.zhoon"